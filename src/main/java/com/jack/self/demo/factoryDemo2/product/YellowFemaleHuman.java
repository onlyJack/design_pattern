package com.jack.self.demo.factoryDemo2.product;

/**
 * Description:
 * Created by jack on 2017/5/8 14:39.
 */
public class YellowFemaleHuman extends YellowHuman {

    @Override
    public void sex() {
        System.out.println("yellowFemale");
    }
}

package com.jack.self.demo.factoryDemo2.product;

/**
 * Description:
 * Created by jack on 2017/5/8 13:36.
 */
public interface Human<V> {

    void laugh();

    void cry();

    void talk();

    void sex();

}

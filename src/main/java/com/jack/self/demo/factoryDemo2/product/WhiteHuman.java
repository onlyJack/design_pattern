package com.jack.self.demo.factoryDemo2.product;

/**
 * Description:
 * Created by jack on 2017/5/8 13:40.
 */
public abstract class WhiteHuman implements Human {

    @Override
    public void laugh() {
        System.out.println("White laugh");
    }

    @Override
    public void cry() {
        System.out.println("White cry");
    }

    @Override
    public void talk() {
        System.out.println("White talk");
    }

}

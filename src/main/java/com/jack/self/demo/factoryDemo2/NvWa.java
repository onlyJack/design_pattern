package com.jack.self.demo.factoryDemo2;

import com.jack.self.demo.factoryDemo2.factory.FactoryManager;
import com.jack.self.demo.factoryDemo2.factory.HumanFactory;
import com.jack.self.demo.factoryDemo2.factory.MaleHumanFactory;
import com.jack.self.demo.factoryDemo2.product.Human;

/**
 * Description:
 * Created by jack on 2017/5/8 13:45.
 */
public class NvWa {
    public static void main(String[] args) {
        //第一条生产线，男性生产线
        HumanFactory maleHumanFactory = new FactoryManager().getFactory(MaleHumanFactory.class);

        Human maleYellowHuman = maleHumanFactory.createYellowHuman();

        maleYellowHuman.cry();
    }
}

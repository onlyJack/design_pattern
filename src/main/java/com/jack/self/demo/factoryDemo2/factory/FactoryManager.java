package com.jack.self.demo.factoryDemo2.factory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * Created by jack on 2017/5/8 14:51.
 */
public class FactoryManager {

    private final Map<Class<?>, HumanFactory> factoryHashMap;

    {
        HashMap<Class<?>, HumanFactory> map = new HashMap<>();
        map.put(MaleHumanFactory.class, new MaleHumanFactory());
        factoryHashMap = Collections.unmodifiableMap(map);
    }

    public HumanFactory getFactory(Class<? extends HumanFactory> humanFactoryClass) {
        return factoryHashMap.get(humanFactoryClass);
    }
}

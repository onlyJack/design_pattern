package com.jack.self.demo.factoryDemo2.factory;

import com.jack.self.demo.factoryDemo2.product.Human;

/**
 * Description:
 * Created by jack on 2017/5/8 13:42.
 */
public interface HumanFactory {

    Human createYellowHuman();

    Human createWhiteHuman();
}

package com.jack.self.demo.factoryDemo2.factory;

import com.jack.self.demo.factoryDemo2.product.Human;

/**
 * Description:
 * Created by jack on 2017/5/8 14:44.
 */
public abstract class AbstractHumanFactory implements HumanFactory {

    protected Human createHuman(Class<? extends Human> aClass) {

        try {
            return aClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}

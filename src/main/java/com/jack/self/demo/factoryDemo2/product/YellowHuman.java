package com.jack.self.demo.factoryDemo2.product;

/**
 * Description:
 * Created by jack on 2017/5/8 13:38.
 */
public abstract class YellowHuman<T> implements Human<T> {

    @Override
    public void laugh() {
        System.out.println("Yellow laugh");
    }

    @Override
    public void cry() {
        System.out.println("Yellow cry");
    }

    @Override
    public void talk() {
        System.out.println("Yello talk");
    }
}

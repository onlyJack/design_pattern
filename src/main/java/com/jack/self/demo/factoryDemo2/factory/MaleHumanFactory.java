package com.jack.self.demo.factoryDemo2.factory;

import com.jack.self.demo.factoryDemo2.product.Human;
import com.jack.self.demo.factoryDemo2.product.YellowMaleHuman;

/**
 * Description:
 * Created by jack on 2017/5/8 14:47.
 */
public class MaleHumanFactory extends AbstractHumanFactory {
    @Override
    public Human createYellowHuman() {
        return super.createHuman(YellowMaleHuman.class);
    }

    @Override
    public Human createWhiteHuman() {
        return null;
    }
}

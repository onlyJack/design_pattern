package com.jack.self.demo.factoryDemo2.product;

/**
 * Description:
 * Created by jack on 2017/5/8 14:40.
 */
public class YellowMaleHuman extends YellowHuman<YellowMaleHuman> {
    @Override
    public void sex() {
        System.out.println("yellowMale");
    }
}

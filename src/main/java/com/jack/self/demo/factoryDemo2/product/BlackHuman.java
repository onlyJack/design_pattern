package com.jack.self.demo.factoryDemo2.product;

/**
 * Description:
 * Created by jack on 2017/5/8 13:41.
 */
public abstract class BlackHuman implements Human {
    @Override
    public void laugh() {
        System.out.println("Black laugh");
    }

    @Override
    public void cry() {
        System.out.println("Black cry");
    }

    @Override
    public void talk() {
        System.out.println("Black talk");
    }
}

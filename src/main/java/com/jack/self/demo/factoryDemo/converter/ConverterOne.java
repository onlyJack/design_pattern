package com.jack.self.demo.factoryDemo.converter;

/**
 * Description:
 * Created by jack on 2017/5/7 10:49.
 */
public class ConverterOne implements Converter<String> {

    private static final ConverterOne converterOne = new ConverterOne();

    private ConverterOne() {
    }

    public static Converter instance() {
        return converterOne;
    }


    @Override
    public <V> String convert(Class<V> vClass) {
        return "abcd";
    }



}

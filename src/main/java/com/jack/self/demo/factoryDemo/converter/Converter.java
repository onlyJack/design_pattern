package com.jack.self.demo.factoryDemo.converter;

/**
 * Description:
 * Created by jack on 2017/5/7 10:49.
 */
@FunctionalInterface
public interface Converter<K> {

    <V> K convert(Class<V> vClass);

}

package com.jack.self.demo.factoryDemo.converter;

/**
 * Description:
 * Created by jack on 2017/5/7 10:49.
 */
public class ConverterTwo implements Converter<Integer> {
    private static final ConverterTwo converterTwo = new ConverterTwo();

    private ConverterTwo() {
    }

    public static ConverterTwo instance(){
        return converterTwo;
    }

    @Override
    public <V> Integer convert(Class<V> vClass) {
        return 1234;
    }
}

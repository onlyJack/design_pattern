package com.jack.self.demo.factoryDemo;

import com.jack.self.demo.factoryDemo.converter.Converter;
import com.jack.self.demo.factoryDemo.converter.ConverterOne;
import com.jack.self.demo.factoryDemo.converter.ConverterTwo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * Created by jack on 2017/5/7 10:48.
 */
public class ConverterManager {

    private static ConverterManager converterManager;

    private final Map<Class<?>, Converter> factoryMap;

    {
        HashMap<Class<?>, Converter> map = new HashMap<>();
        map.put(String.class, ConverterOne.instance());
        map.put(Integer.class, ConverterTwo.instance());
        factoryMap = Collections.unmodifiableMap(map);
    }

    private ConverterManager() {
    }

    public static ConverterManager instance() {
        if (converterManager == null) {
            synchronized (ConverterManager.class) {
                if (converterManager == null) {
                    converterManager = new ConverterManager();
                }
            }
        }
        return converterManager;
    }

    @SuppressWarnings("unchecked")
    public <K> Converter<K> getConverter(Class<K> kClass) {
        return factoryMap.get(kClass);
    }
}

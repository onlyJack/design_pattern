package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/3/15 15:03.
 */
public interface Command {
    public void executed();
}

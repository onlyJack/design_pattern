package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/4/22 13:43.
 */
public class Stereo {

    public void on() {
        System.out.println("音响已打开");
    }

    public void off(){
        System.out.println("音响已关闭");
    }

    public void setCD(){
        System.out.println("播放CD");
    }

    public void setDVD(){
        System.out.println("播放DBD");
    }

    public void setRadio(){
        System.out.println("播放收音机");
    }

    public void setVolume(int volume) {
        System.out.println("设置音量为：" + volume);
    }
}

package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/4/22 13:40.
 */
public class LightOffCommand implements Command {

    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void executed() {
        light.off();
    }
}

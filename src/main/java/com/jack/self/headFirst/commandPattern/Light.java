package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/3/15 15:05.
 */
public class Light {

    private String lightType;

    public void on() {
        System.out.println("电灯已打开");
    }

    public void off(){
        System.out.println("电灯已关闭");
    }

    public Light() {
    }

    public Light(String lightType) {
        this.lightType = lightType;
    }

    public String getLightType() {
        return lightType;
    }

    public void setLightType(String lightType) {
        this.lightType = lightType;
    }
}

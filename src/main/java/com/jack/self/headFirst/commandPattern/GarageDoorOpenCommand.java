package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/4/20 22:32.
 */
public class GarageDoorOpenCommand implements Command {

    GarageDoor garageDoor;

    public GarageDoorOpenCommand(GarageDoor garageDoor) {
        this.garageDoor = garageDoor;
    }

    @Override
    public void executed() {
        garageDoor.open();
    }
}

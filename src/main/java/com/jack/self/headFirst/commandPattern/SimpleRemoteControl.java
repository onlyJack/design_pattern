package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/3/15 15:08.
 */
public class SimpleRemoteControl {

    private Command slot;

    public SimpleRemoteControl() {
    }

    public void setCommand(Command command) {
        slot = command;
    }

    public void buttonWasPressed(){
        slot.executed();
    }
}

package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/3/15 15:04.
 */
public class LightOnCommand implements Command {

    Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void executed() {
        light.on();
    }
}

package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/4/22 12:02.
 */
public class NoCommand implements Command {

    @Override
    public void executed() {

    }
}

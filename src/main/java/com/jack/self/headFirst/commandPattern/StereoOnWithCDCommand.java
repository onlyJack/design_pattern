package com.jack.self.headFirst.commandPattern;

/**
 * Description:
 * Created by jack on 2017/4/22 14:52.
 */
public class StereoOnWithCDCommand implements Command {

    private Stereo stereo;

    public StereoOnWithCDCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void executed() {
        stereo.on();
        stereo.setCD();
        stereo.setVolume(11);
    }
}

package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.sauce;

/**
 * Description:
 * Created by jack on 2017/1/11 22:22.
 */
public interface Sauce {
    public void createSauce();
}

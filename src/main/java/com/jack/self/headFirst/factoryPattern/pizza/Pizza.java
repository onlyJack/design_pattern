package com.jack.self.headFirst.factoryPattern.pizza;

import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.cheese.Cheese;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.clams.Clams;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.dough.Dough;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.pepperoni.Pepperoni;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.sauce.Sauce;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.viggies.Veggies;

import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 * Created by jack on 2017/1/4 22:10.
 */
public abstract class Pizza {
    String name;

    Dough dough;

    Sauce sauce;

    Veggies veggies[];

    Cheese cheese;

    Pepperoni pepperoni;

    Clams clams;

    List<String> toppings = new ArrayList<>();

    /**
     * 抽象工厂模式prepare
     */
    public abstract void prepare();

    /**
     * 工厂方法模式prepare
     */
    /*public void prepare() {
        System.out.println("Preparing " + name);
        System.out.println("Tossing dough...");
        System.out.println("Adding sauce...");
        System.out.println("Adding toppings: ");
        toppings.forEach(topping ->
                System.out.println("    " + topping)
        );
    }*/

    public void bake(){
        System.out.println("Bake for 25 minutes at 350");
    }

    public void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    public void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

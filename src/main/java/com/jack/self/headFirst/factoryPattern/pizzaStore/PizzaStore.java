package com.jack.self.headFirst.factoryPattern.pizzaStore;

import com.jack.self.headFirst.factoryPattern.pizza.Pizza;

/**
 * Description:
 * Created by jack on 2017/1/4 22:23.
 */
public abstract class PizzaStore {
    public Pizza orderPizza(String type) {
        Pizza pizza;
        pizza = createPizza(type);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }

    abstract Pizza createPizza(String type);
}

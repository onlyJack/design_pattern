package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.clams;

/**
 * Description:
 * Created by jack on 2017/1/11 22:24.
 */
public interface Clams {
    public void createClams();
}

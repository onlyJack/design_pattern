package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.dough;

/**
 * Description:
 * Created by jack on 2017/1/11 22:21.
 */
public interface Dough {
    public void createDough();
}

package com.jack.self.headFirst.factoryPattern.pizza;

import com.jack.self.headFirst.factoryPattern.ingredient.PizzaIngredientFactory;

/**
 * Description:
 * Created by jack on 2017/2/27 16:39.
 */
public class CheesePizza extends Pizza {

    PizzaIngredientFactory ingredientFactory;

    public CheesePizza (PizzaIngredientFactory ingredientFactory){
        this.ingredientFactory = ingredientFactory;
    }
    @Override
    public void prepare() {
        System.out.println("Preparing " + name);
        dough = ingredientFactory.createDough();
        sauce = ingredientFactory.createSauce();
        cheese = ingredientFactory.createCheese();
    }
}

package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.dough;

/**
 * Description:
 * Created by jack on 2017/1/11 22:37.
 */
public class ThincrustDough implements Dough {

    @Override
    public void createDough() {
        System.out.println("ThicrustDought");
    }
}

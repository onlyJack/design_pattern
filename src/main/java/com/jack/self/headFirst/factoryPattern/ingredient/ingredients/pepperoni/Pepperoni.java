package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.pepperoni;

/**
 * Description:
 * Created by jack on 2017/1/11 22:23.
 */
public interface Pepperoni {
    public void createPepperoni();
}

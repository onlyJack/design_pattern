package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.cheese;

/**
 * Description:
 * Created by jack on 2017/1/23 22:11.
 */
public class ReggianoCheese implements Cheese {

    @Override
    public void createCheese() {
        System.out.println("ReggianoCheese");
    }
}

package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.viggies;

/**
 * Description:
 * Created by jack on 2017/1/23 22:16.
 */
public class Mushroom implements Veggies {
    @Override
    public void createVeggies() {
        System.out.println("Mushroom");
    }
}

package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.cheese;

/**
 * Description:
 * Created by jack on 2017/1/11 22:22.
 */
public interface Cheese {
    public void createCheese();
}

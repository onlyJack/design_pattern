package com.jack.self.headFirst.factoryPattern.ingredient;

import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.cheese.Cheese;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.cheese.ReggianoCheese;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.clams.Clams;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.clams.FreshClams;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.dough.Dough;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.dough.ThincrustDough;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.pepperoni.Pepperoni;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.pepperoni.SlicedPepperoni;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.sauce.MarinaraSauce;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.sauce.Sauce;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.viggies.*;

/**
 * Description:
 * Created by jack on 2017/1/11 22:29.
 */
public class NYPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new ThincrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new MarinaraSauce();
    }

    @Override
    public Cheese createCheese() {
        return new ReggianoCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        return new Veggies[]{new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
    }

    @Override
    public Pepperoni createPepperoni() {
        return new SlicedPepperoni();
    }

    @Override
    public Clams createClam() {
        return new FreshClams();
    }
}

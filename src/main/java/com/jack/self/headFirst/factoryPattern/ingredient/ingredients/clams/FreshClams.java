package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.clams;

/**
 * Description:
 * Created by jack on 2017/1/23 22:25.
 */
public class FreshClams implements Clams{
    @Override
    public void createClams() {
        System.out.println("FreeshClams");
    }
}

package com.jack.self.headFirst.factoryPattern.ingredient;

import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.cheese.Cheese;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.clams.Clams;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.dough.Dough;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.pepperoni.Pepperoni;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.sauce.Sauce;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.viggies.Veggies;

/**
 * Description:
 * Created by jack on 2017/1/11 22:19.
 */
public interface PizzaIngredientFactory {

    Dough createDough();

    Sauce createSauce();

    Cheese createCheese();

    Veggies[] createVeggies();

    Pepperoni createPepperoni();

    Clams createClam();
}

package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.viggies;

/**
 * Description:
 * Created by jack on 2017/1/11 22:22.
 */
public interface Veggies {
    public void createVeggies();
}

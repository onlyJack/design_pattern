package com.jack.self.headFirst.factoryPattern.ingredient;

import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.cheese.Cheese;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.clams.Clams;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.dough.Dough;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.pepperoni.Pepperoni;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.sauce.Sauce;
import com.jack.self.headFirst.factoryPattern.ingredient.ingredients.viggies.Veggies;

/**
 * Description:
 * Created by jack on 2017/1/23 22:05.
 */
public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory{

    @Override
    public Dough createDough() {
        return null;
    }

    @Override
    public Sauce createSauce() {
        return null;
    }

    @Override
    public Cheese createCheese() {
        return null;
    }

    @Override
    public Veggies[] createVeggies() {
        return new Veggies[0];
    }

    @Override
    public Pepperoni createPepperoni() {
        return null;
    }

    @Override
    public Clams createClam() {
        return null;
    }
}

package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.pepperoni;

/**
 * Description:
 * Created by jack on 2017/1/23 22:19.
 */
public class SlicedPepperoni implements  Pepperoni {

    @Override
    public void createPepperoni() {
        System.out.println("SlicedPepperoni");
    }
}

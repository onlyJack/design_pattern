package com.jack.self.headFirst.factoryPattern.pizzaStore;

import com.jack.self.headFirst.factoryPattern.ingredient.NYPizzaIngredientFactory;
import com.jack.self.headFirst.factoryPattern.ingredient.PizzaIngredientFactory;
import com.jack.self.headFirst.factoryPattern.pizza.CheesePizza;
import com.jack.self.headFirst.factoryPattern.pizza.Pizza;

/**
 * Description:
 * Created by jack on 2017/3/6 11:45.
 */
public class NYPizzaStroe extends PizzaStore {

    @Override
    protected Pizza createPizza(String type) {
        Pizza pizza = null;
        PizzaIngredientFactory ingredientFactory = new NYPizzaIngredientFactory();

        if (type.equals("cheese")) {
            pizza = new CheesePizza(ingredientFactory);
            pizza.setName("New York Style Cheese Pizza");
        }
        if (type.equals("veggie")) {
//            pizza = new VeggiesPizza();
            pizza.setName("New York Style Veggie Pizza");
        }
        return pizza;
    }
}

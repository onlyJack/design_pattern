package com.jack.self.headFirst.factoryPattern.ingredient.ingredients.sauce;

/**
 * Description:
 * Created by jack on 2017/1/23 22:10.
 */
public class MarinaraSauce implements Sauce {

    @Override
    public void createSauce() {
        System.out.println("MarinaraSauce");
    }
}

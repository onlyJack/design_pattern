package com.jack.self.headFirst.factoryPattern.pizza;

import com.jack.self.headFirst.factoryPattern.ingredient.PizzaIngredientFactory;

/**
 * Description:
 * Created by jack on 2017/3/6 11:25.
 */
public class ClamPizza extends Pizza {

    PizzaIngredientFactory ingredientFactory;

    public ClamPizza(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }

    @Override
    public void prepare() {
        System.out.println("Preparing " + name);
        dough = ingredientFactory.createDough();
        sauce = ingredientFactory.createSauce();
        cheese = ingredientFactory.createCheese();
        clams = ingredientFactory.createClam();
    }
}

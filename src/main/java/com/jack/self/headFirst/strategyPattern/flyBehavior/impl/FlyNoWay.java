package com.jack.self.headFirst.strategyPattern.flyBehavior.impl;

import com.jack.self.headFirst.strategyPattern.flyBehavior.FlyBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:31.
 */
public class FlyNoWay implements FlyBehavior {

    public void fly() {
        System.out.println("不会飞翔！");
    }
}

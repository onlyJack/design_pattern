package com.jack.self.headFirst.strategyPattern.duck;

import com.jack.self.headFirst.strategyPattern.flyBehavior.impl.FlyNoWay;
import com.jack.self.headFirst.strategyPattern.quackBehavior.impl.Quack;

/**
 * Description:
 * Created by jack on 2016/12/18 11:46.
 */
public class ModelDuck extends Duck {

    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    @Override
    public void display() {
        System.out.println("我是一只模型鸭子！");
    }
}

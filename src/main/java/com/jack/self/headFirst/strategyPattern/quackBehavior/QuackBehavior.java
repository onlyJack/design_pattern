package com.jack.self.headFirst.strategyPattern.quackBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:32.
 */
public interface QuackBehavior {
    public void quack();
}

package com.jack.self.headFirst.strategyPattern.flyBehavior.impl;

import com.jack.self.headFirst.strategyPattern.flyBehavior.FlyBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:30.
 */
public class FlyWithWings implements FlyBehavior {

    public void fly() {
        System.out.println("飞翔！");
    }
}

package com.jack.self.headFirst.strategyPattern.quackBehavior.impl;

import com.jack.self.headFirst.strategyPattern.quackBehavior.QuackBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:35.
 */
public class Squeak implements QuackBehavior {
    public void quack() {
        System.out.println("唧唧叫！");
    }
}

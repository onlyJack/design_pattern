package com.jack.self.headFirst.strategyPattern.flyBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:29.
 */
public interface FlyBehavior {
    public void fly();
}

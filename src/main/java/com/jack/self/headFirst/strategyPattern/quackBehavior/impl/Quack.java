package com.jack.self.headFirst.strategyPattern.quackBehavior.impl;

import com.jack.self.headFirst.strategyPattern.quackBehavior.QuackBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:33.
 */
public class Quack implements QuackBehavior {
    public void quack() {
        System.out.println("嘎嘎叫！");
    }
}

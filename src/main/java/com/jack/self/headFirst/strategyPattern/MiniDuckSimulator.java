package com.jack.self.headFirst.strategyPattern;

import com.jack.self.headFirst.strategyPattern.duck.Duck;
import com.jack.self.headFirst.strategyPattern.duck.MalardDuck;
import com.jack.self.headFirst.strategyPattern.duck.ModelDuck;
import com.jack.self.headFirst.strategyPattern.flyBehavior.impl.FlyRocketPowered;

/**
 * Description; 策略模式测试类。
 * Created by jack on 2016/12/7 14:55.
 */
public class MiniDuckSimulator {

    public static void main(String... args) {
        Duck mallard = new MalardDuck();
        mallard.performQuack();
        mallard.performFly();
        mallard.display();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
        model.display();
    }
}

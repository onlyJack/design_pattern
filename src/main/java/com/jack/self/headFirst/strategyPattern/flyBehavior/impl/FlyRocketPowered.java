package com.jack.self.headFirst.strategyPattern.flyBehavior.impl;

import com.jack.self.headFirst.strategyPattern.flyBehavior.FlyBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:48.
 */
public class FlyRocketPowered implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("我可以乘坐火箭飞行！");
    }
}

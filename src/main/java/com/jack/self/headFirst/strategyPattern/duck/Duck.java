package com.jack.self.headFirst.strategyPattern.duck;

import com.jack.self.headFirst.strategyPattern.flyBehavior.FlyBehavior;
import com.jack.self.headFirst.strategyPattern.quackBehavior.QuackBehavior;

/**
 * Description:
 * Created by jack on 2016/12/18 11:36.
 */
public abstract class Duck {
    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;
    public Duck(){

    }

    public abstract void display();

    public void performFly(){
        flyBehavior.fly();
    }

    public void performQuack(){
        quackBehavior.quack();
    }

    public void swim(){
        System.out.println("所有鸭子都会有用，即使是假鸭子！");
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}

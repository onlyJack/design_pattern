package com.jack.self.headFirst.strategyPattern.duck;

import com.jack.self.headFirst.strategyPattern.flyBehavior.impl.FlyWithWings;
import com.jack.self.headFirst.strategyPattern.quackBehavior.impl.Quack;

/**
 * Description:
 * Created by jack on 2016/12/18 11:41.
 */
public class MalardDuck extends Duck {

    public MalardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    public void display() {
        System.out.println("我是一只真正的绿头鸭子！");
    }
}

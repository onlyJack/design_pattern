package com.jack.self.headFirst.observer.interfaces;

/**
 * Description: 观察者接口
 * Created by jack on 2016/12/18 15:55.
 */
public interface Observer {
    public void update(float temp, float humidity, float pressure); //当气象观察值改变时将这些数据传送给观察者
}

package com.jack.self.headFirst.observer;

import com.jack.self.headFirst.observer.interfaces.Observer;
import com.jack.self.headFirst.observer.interfaces.Subject;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Description:
 * Created by jack on 2016/12/18 16:08.
 */
public class WeatherData implements Subject {
    private ArrayList<Observer> observers;
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        int i = this.observers.indexOf(observer);
        if (i >= 0) {
            this.observers.remove(i);
        }
    }

    @Override
    public void notifyObservers() {
//        this.observers.forEach(observer -> observer.update(this.temperature, this.humidity, this.pressure));

        this.observers = this.observers.stream()
                .peek(observer -> observer.update(this.temperature, this.humidity, this.pressure))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public void measurementsChanged() {
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }
}

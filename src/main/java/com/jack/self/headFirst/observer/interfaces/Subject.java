package com.jack.self.headFirst.observer.interfaces;

/**
 * Description: 主题接口
 * Created by jack on 2016/12/18 15:54.
 */
public interface Subject {
    public void registerObserver(Observer observer);    // 注册观察者

    public void removeObserver(Observer observer);      //删除观察者

    public void notifyObservers();      //当主题改变时，调用这个方法通知所有观察者

}

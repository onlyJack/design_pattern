package com.jack.self.headFirst.observer.interfaces;

/**
 * Description: 展示接口
 * Created by jack on 2016/12/18 15:57.
 */
public interface DisplayElement {
    public void display();  //布告版展示时调用当方法
}

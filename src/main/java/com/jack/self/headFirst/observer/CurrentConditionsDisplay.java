package com.jack.self.headFirst.observer;

import com.jack.self.headFirst.observer.interfaces.DisplayElement;
import com.jack.self.headFirst.observer.interfaces.Observer;
import com.jack.self.headFirst.observer.interfaces.Subject;

/**
 * Description:
 * Created by jack on 2016/12/18 16:21.
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement{

    private float temperature;
    private float humidity;
private Subject weatherData;

    public CurrentConditionsDisplay(Subject weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    @Override
    public void update(float temp, float humidity, float pressure) {
        this.temperature = temp;
        this.humidity = humidity;
        display();
    }

    @Override
    public void display() {
        System.out.println("当前温度：" + this.temperature
                + "摄氏度，湿度为：" + this.humidity + "%。");
    }
}

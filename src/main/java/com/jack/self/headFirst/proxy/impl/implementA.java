package com.jack.self.headFirst.proxy.impl;

import com.jack.self.headFirst.proxy.proxyEntity;

/**
 * Description:
 * Created by jack on 2016/12/8 19:34.
 */
public class implementA implements proxyEntity {
    public void function1() {
        System.out.println("This is implementA function1");
    }

    public void function2() {
        System.out.println("This is implementA function2");
    }
}

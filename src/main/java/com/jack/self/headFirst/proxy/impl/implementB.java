package com.jack.self.headFirst.proxy.impl;

import com.jack.self.headFirst.proxy.proxyEntity;

/**
 * Description:
 * Created by jack on 2016/12/8 19:37.
 */
public class implementB implements proxyEntity {
    public void function1() {
        System.out.println("This is implementB function1");
    }

    public void function2() {
        System.out.println("This is implementB function2");
    }
}

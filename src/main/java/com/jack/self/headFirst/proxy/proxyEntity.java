package com.jack.self.headFirst.proxy;

/**
 * Description:
 * Created by jack on 2016/12/8 19:30.
 */
public interface proxyEntity {

    void function1();

    void function2();
}

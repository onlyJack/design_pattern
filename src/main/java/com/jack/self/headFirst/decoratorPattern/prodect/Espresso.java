package com.jack.self.headFirst.decoratorPattern.prodect;

import com.jack.self.headFirst.decoratorPattern.Beverage;

/**
 * Description:
 * Created by jack on 2017/1/2 23:03.
 */
public class Espresso extends Beverage {

    public Espresso(SizeEnum size) {
        description = "Espresso";
        this.size = size;
    }

    public double cost() {
        if (size.equals(SizeEnum.TALL)) {
            return 1.99;
        } else if (size.equals(SizeEnum.GRANDE)) {
            return 2.19;
        } else if (size.equals(SizeEnum.VENTI)) {
            return 2.39;
        }
        return 0;
    }
}

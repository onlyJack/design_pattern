package com.jack.self.headFirst.decoratorPattern.condiment.decorator;

import com.jack.self.headFirst.decoratorPattern.Beverage;
import com.jack.self.headFirst.decoratorPattern.condiment.CondimentDecorator;
import com.jack.self.headFirst.decoratorPattern.prodect.SizeEnum;

/**
 * Description:
 * Created by jack on 2017/1/3 19:08.
 */
public class Soy extends CondimentDecorator {

    Beverage beverage;

    public Soy(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", roy";
    }

    @Override
    public SizeEnum getSize() {
        return beverage.getSize();
    }

    @Override
    public double cost() {
        return .31 + beverage.cost();
    }
}

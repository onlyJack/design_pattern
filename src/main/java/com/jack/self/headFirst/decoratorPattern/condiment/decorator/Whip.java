package com.jack.self.headFirst.decoratorPattern.condiment.decorator;

import com.jack.self.headFirst.decoratorPattern.Beverage;
import com.jack.self.headFirst.decoratorPattern.condiment.CondimentDecorator;
import com.jack.self.headFirst.decoratorPattern.prodect.SizeEnum;

/**
 * Description:
 * Created by jack on 2017/1/3 19:10.
 */
public class Whip extends CondimentDecorator {

    Beverage beverage;

    public Whip(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", whip";
    }

    @Override
    public SizeEnum getSize() {
        return beverage.getSize();
    }

    @Override
    public double cost() {
        double cost = beverage.cost();
        if (getSize().equals(SizeEnum.TALL)) {
            cost += .1;
        } else if (getSize().equals(SizeEnum.GRANDE)) {
            cost += .2;
        } else if (getSize().equals(SizeEnum.VENTI)) {
            cost += .3;
        }
        return cost;
    }
}

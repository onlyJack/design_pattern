package com.jack.self.headFirst.decoratorPattern;

import com.jack.self.headFirst.decoratorPattern.prodect.SizeEnum;

/**
 * Description:
 * Created by jack on 2017/1/2 22:57.
 */
public abstract class Beverage{

    protected String description = "Unknown Beverage";

    protected SizeEnum size;

    public String getDescription() {
        return description;
    }

    public SizeEnum getSize() {
        return size;
    }

    public abstract double cost();
    
}

package com.jack.self.headFirst.decoratorPattern.prodect;

import com.jack.self.headFirst.decoratorPattern.Beverage;

/**
 * Description:
 * Created by jack on 2017/1/3 19:05.
 */
public class DarkRoash extends Beverage {

    public DarkRoash() {
        description = "Dark Roash";
    }

    @Override
    public double cost() {
        return 1.28;
    }

}

package com.jack.self.headFirst.decoratorPattern;

import com.jack.self.headFirst.decoratorPattern.condiment.decorator.Mocha;
import com.jack.self.headFirst.decoratorPattern.condiment.decorator.Soy;
import com.jack.self.headFirst.decoratorPattern.condiment.decorator.Whip;
import com.jack.self.headFirst.decoratorPattern.prodect.DarkRoash;
import com.jack.self.headFirst.decoratorPattern.prodect.Espresso;
import com.jack.self.headFirst.decoratorPattern.prodect.HouseBlend;
import com.jack.self.headFirst.decoratorPattern.prodect.SizeEnum;

/**
 * Description:
 * Created by jack on 2017/1/2 23:13.
 */
public class Test {
    public static void main(String args[]) {
        Beverage beverage = new Espresso(SizeEnum.GRANDE);
        beverage = new Whip(beverage);
        beverage = new Whip(beverage);
        System.out.println(beverage.getDescription() + " $" + beverage.cost());

        Beverage beverage2 = new DarkRoash();
        beverage2 = new Mocha(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println(beverage2.description + ", $" + beverage2.cost());

        Beverage beverage3 = new HouseBlend();
        beverage3 = new Soy(beverage3);
        beverage3 = new Mocha(beverage3);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription() + " $" + beverage3.cost());
    }
}

package com.jack.self.headFirst.decoratorPattern.prodect;

import com.jack.self.headFirst.decoratorPattern.Beverage;

/**
 * Description:
 * Created by jack on 2017/1/2 23:06.
 */
public class HouseBlend extends Beverage {

    public HouseBlend() {
        description = "House Blend Coffee";
    }

    public double cost() {
        return 0.89;
    }

}

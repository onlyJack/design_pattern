package com.jack.self.headFirst.decoratorPattern.condiment;

import com.jack.self.headFirst.decoratorPattern.Beverage;
import com.jack.self.headFirst.decoratorPattern.prodect.SizeEnum;

/**
 * Description:
 * Created by jack on 2017/1/2 23:00.
 */
public abstract class CondimentDecorator extends Beverage {
    public abstract String getDescription();
    public abstract SizeEnum getSize();
}

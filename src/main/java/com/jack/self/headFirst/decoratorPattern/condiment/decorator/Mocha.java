package com.jack.self.headFirst.decoratorPattern.condiment.decorator;

import com.jack.self.headFirst.decoratorPattern.Beverage;
import com.jack.self.headFirst.decoratorPattern.condiment.CondimentDecorator;
import com.jack.self.headFirst.decoratorPattern.prodect.SizeEnum;

/**
 * Description:
 * Created by jack on 2017/1/2 23:09.
 */
public class Mocha extends CondimentDecorator {

    Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    public String getDescription() {
        return beverage.getDescription() + ", Mocha";
    }

    @Override
    public SizeEnum getSize() {
        return beverage.getSize();
    }

    public double cost() {
        return .20 + beverage.cost();
    }
}

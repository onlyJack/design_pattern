package com.jack.self.headFirst.singletonPattern;

/**
 * Description:
 * Created by jack on 2017/3/7 14:39.
 */
public class Singleton {
    private static Singleton uniqueInstance;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }
}
